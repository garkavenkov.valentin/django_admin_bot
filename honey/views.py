from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from .models import *

# Create your views here.


def base(request: HttpRequest) -> HttpResponse:
    catalog = Base.objects.filter(available=1)
    return render(request, "honey/base.html", {"catalog": catalog})


def questions(request: HttpRequest) -> HttpResponse:
    catalog_q = Question.objects.all()
    return render(request, "honey/base.html", {"catalog": catalog_q})


def show_post(request: HttpRequest, id) -> HttpResponse:
    catalog = Base.objects.filter(id=id)
    return render(request, "honey/honey.html", {"catalog": catalog})
