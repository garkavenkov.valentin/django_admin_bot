from django.contrib import admin
from .models import Base, Question, Orders


@admin.register(Base)
class BaseAdmin(admin.ModelAdmin):
    list_display = ["name", "category", "year", "price", "available"]
    list_editable = ["year", "price", "available"]
    ordering = ["category"]
    list_per_page = 15


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ["product_name", "client_name", "telegram", "time_create"]


@admin.register(Orders)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ["client_name", "product_name", "amount"]