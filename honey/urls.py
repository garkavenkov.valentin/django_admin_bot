from django.urls import path
from . import views


urlpatterns = [
    path("", views.base, name="base"),
    path("questions/", views.questions, name="questions"),
    path("post/<int:id>/", views.show_post, name='post')
]
