from django.db import models


class Base(models.Model):
    category = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    price = models.IntegerField(default=0)
    year = models.IntegerField(default=2022)
    photo = models.ImageField(upload_to="images/")
    available = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.name


class Question(models.Model):
    telegram = models.IntegerField(default=0)
    product_name = models.ForeignKey(Base, on_delete=models.CASCADE)
    question = models.TextField(blank=True)
    time_create = models.DateTimeField(auto_now_add=True)
    client_name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.question


class Orders(models.Model):
    telegram_id = models.IntegerField(default=0)
    product_name = models.ForeignKey(Base, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    client_name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.client_name
